[![Troon](https://hqdemo.com//projects-documentation/img/logo.svg "Troon")](https://troontechnologies.com "Troon")

Troon Markdown editor
=====================
Markdown editor powered by [Troon Technologies](https://troontechnologies.com/)

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist troon-markdown/yii2-troon-markdown "*"
```

or add

```
"troon-markdown/yii2-troon-markdown": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php

<?php
  use troon\markdown\EditorMdWidget;
echo $form->field($model, 'content')->widget(EditorMdWidget::className(), [
        'options'=>[
            'id'=>'content'
        ],
        'clientOptions' => [
            'height' => '640',
            // 'previewTheme' => 'dark',
            // 'editorTheme' => 'pastel-on-dark',
            'markdown' => '',
            'codeFold' => true,
            'syncScrolling' => false,
            'saveHTMLToTextarea' => true,
            'searchReplace' => true,
            'htmlDecode' => 'style,script,iframe|on*', 
            'toolbar ' => false, 
            'previewCodeHighlight' => false,
            'emoji' => true,
            'taskList' => true,
            'tocm' => true, 
            'tex' => true,   
            'flowChart' => true,          
            'sequenceDiagram' => true,      
            'imageUpload' => true,
            'imageFormats' => ['jpg', 'jpeg', 'gif', 'png', 'bmp', 'webp'],
            'imageUploadURL' => '/file/blog-upload?type=default&filekey=editormd-image-file',
        ]
    ]);
?>
```

To display Markdown on web view 


```php
<?php
    use troon\markdown\ViewMdWidget;
      echo   ViewMdWidget::widget([
            'markdown'=>$model->content,
            'options'=>[// html attributes
                'id'=>'content'
            ]
        ]);
    ?>
```
