<?php
/**
 * @link https://troontechnologies.com/
 * @copyright Copyright (c) 2020 Troon
 * @license https://troontechnologies.com/
 */
namespace troon\markdown;

use yii\bootstrap\InputWidget;
use troon\markdown\assets\ViewMdAsset;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\HtmlPurifier;

class ViewMdWidget extends Widget
{
    /**
     * editor options
     * @var array
     */
    public $clientOptions = [];
    public $options = [];
    public $markdown = '';


    /**
     * Renders the widget.
     */
    public function run()
    {
        if ($this->markdown) {
             return $this->registerClientScript($this->markdown);
        }

    }

    public function registerClientScript($content, $config = [])
    {

        $view = $this->getView();
        $this->initClientOptions();
        $editor = ViewMdAsset::register($view);
        $this->clientOptions['path'] = $editor->baseUrl . '/lib/';
        $this->clientOptions['markdown'] = $content;
        $jsOptions = Json::encode($this->clientOptions);
        $id = ($this->options['id'])?$this->options['id']:'content';
        if ($this->clientOptions['emoji']) {
            $emoji = 'editormd.emoji = ' . Json::encode(['path' => 'http://www.webpagefx.com/tools/emoji-cheat-sheet/graphics/emojis/', 'ext' => ".png"]);
            $view->registerJs($emoji);
        }
        $js = 'var editor = editormd.markdownToHTML("' . $id . '", '.$jsOptions.');';
        $view->registerJs($js);
        $content = '<div id="content"><textarea id="append-test" style="display:none;"></textarea></div>';
        return $content;
    }

    public function initClientOptions()
    {
        $options = [];
        $options['height'] = '600';
        $options['watch'] = true;
        $options['emoji'] = true;
        $options['htmlDecode'] = "style,script,iframe";
        $options['taskList'] = true;
        $options['tex'] = true;
        $options['flowChart'] = true;
        $options['sequenceDiagram'] = true;
        $this->clientOptions = array_merge($options, $this->clientOptions);
    }
}
