<?php

namespace troon\markdown\assets;

use yii\web\AssetBundle;

class EditorMdAsset extends AssetBundle
{
//    public $sourcePath = '@bower/editor.md';
    public $sourcePath = '@vendor/troon-markdown/yii2-troon-markdown/editor.md';

    public function init()
    {
        $this->css = ['css/editormd.css', 'css/editormd.logo.css', 'css/editormd.preview.css'];
        $this->js = ['editormd.js'];
    }

}
