<?php

namespace troon\markdown\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;

class LangMdAsset extends AssetBundle
{
    public $sourcePath = '@vendor/troon-markdown/yii2-troon-markdown/assets/lang';

    /**
     * @inheritdoc
     */
    public $js = [];

    public function registerLanguage($lang){
        $this->js[] = $lang.'.js';
    }

}
