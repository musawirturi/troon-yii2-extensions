<?php

namespace troon\markdown\assets;

use yii\web\AssetBundle;

class ViewMdAsset extends AssetBundle
{
//    public $sourcePath = '@bower/editor.md';
    public $sourcePath = '@vendor/troon-markdown/yii2-troon-markdown/editor.md';
    public function init()
    {
        $this->css = ['css/editormd.css', 'css/editormd.logo.css', 'css/editormd.preview.css'];
        $this->js = [
            'editormd.js',
            'lib/marked.min.js',
            'lib/prettify.min.js',
            'lib/raphael.min.js',
            'lib/underscore.min.js',
            'lib/sequence-diagram.min.js',
            'lib/flowchart.min.js',
            'lib/jquery.flowchart.min.js',
            ];
    }
}
